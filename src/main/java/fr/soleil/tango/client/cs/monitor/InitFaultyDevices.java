package fr.soleil.tango.client.cs.monitor;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.tango.clientapi.TangoAttribute;
import fr.soleil.tango.clientapi.TangoCommand;

public class InitFaultyDevices {

    private static String deviceName = System.getProperty("cs.monitor.name", "org/tango/monitor");

    public static void main(final String[] args) {
        try {
            final DeviceProxy cs = new DeviceProxy(deviceName);
            final TangoCommand init = new TangoCommand(deviceName, "InitFaultyClass");
            final TangoCommand states = new TangoCommand(deviceName, "getClassState");
            final TangoAttribute lastResult = new TangoAttribute(deviceName + "/lastResult");
            for (int i = 0; i < args.length; i++) {
                System.out.println("******* init class " + args[i] + " *******");
                init.execute(args[i]);
                wait(cs);
                System.out.println("----- result is:");
                System.out.println(lastResult.readAsString("\n", ""));
                System.out.println("----- state of " + args[i] + " is:");
                states.execute(args[i]);
                wait(cs);
                System.out.println(lastResult.readAsString("\n", ""));

            }
        } catch (final DevFailed e) {
            e.printStackTrace();
            DevFailedUtils.printDevFailed(e);
        }

    }

    private static void wait(final DeviceProxy cs) throws DevFailed {
        // wait end execution
        while (cs.state().value() == DevState._RUNNING) {
            try {
                Thread.sleep(100);
            } catch (final InterruptedException e) {
                break;
            }
        }
    }
}
