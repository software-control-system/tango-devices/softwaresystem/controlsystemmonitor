package fr.soleil.tango.server.cs.monitor;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.DeviceState;
import org.tango.server.ServerManager;
import org.tango.server.annotation.Attribute;
import org.tango.server.annotation.Command;
import org.tango.server.annotation.Delete;
import org.tango.server.annotation.Device;
import org.tango.server.annotation.DynamicManagement;
import org.tango.server.annotation.Init;
import org.tango.server.annotation.State;
import org.tango.server.annotation.StateMachine;
import org.tango.server.annotation.Status;
import org.tango.server.annotation.TransactionType;
import org.tango.server.attribute.log.LogAttribute;
import org.tango.server.dynamic.DynamicManager;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DispLevel;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.Group.Group;
import fr.esrf.TangoApi.Group.GroupAttrReply;
import fr.esrf.TangoApi.Group.GroupAttrReplyList;

/**
 * Utility device to monitor a Control System. <br>
 * <br>
 * Can do many things: <br>
 * - get status or states of group of devices<br>
 * - kill/start group of devices<br>
 * - execute group command<br>
 * <br>
 * Each command is available :<br>
 * - either with a device name pattern like ans&#042;/ae/alim&#042;<br>
 * - or by device class name like StateComposer<br>
 * <br>
 */
@Device(transactionType = TransactionType.DEVICE)
public final class ControlSystemMonitor {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    private static final String WAITING_FOR_REQUEST = "Waiting for request";
    private static final int DEFAULT_TIMEOUT = 3000;
    private static final String DESC_1 = "The device pattern (wildcard char is *), timeout (optional)";
    private static final String DESC_2 = "The device class pattern (wildcard char is *), timeout (optional)";
    private final Logger logger = LoggerFactory.getLogger(ControlSystemMonitor.class);
    @Status
    private volatile String status;
    @State
    private volatile DeviceState state;

    @DynamicManagement
    DynamicManager dynamicManager;

    @Attribute
    private volatile String[] lastResult = new String[0];

    @Attribute(name = "historic")
    private String[] historic = null;
    private List<String> historicArrays = new ArrayList<String>();

    private Database tangoDB;
    private Group starterGroup;
    private ControlSystemHelper controlSystemHelper;

    public static void main(final String[] args) {
        ServerManager.getInstance().start(args, ControlSystemMonitor.class);
    }

    @Init(lazyLoading = true)
    @StateMachine(endState = DeviceState.ON)
    public void init() throws DevFailed {
        historicArrays.clear();
        lastResult = new String[0];
        dynamicManager.addAttribute(new LogAttribute(1000, logger));
        tangoDB = ApiUtil.get_db_obj();
        controlSystemHelper = new ControlSystemHelper(tangoDB);
        final String[] servers = tangoDB.get_server_list();
        final String[] classes = tangoDB.get_class_list("*");
        logger.debug("connection to tango db OK. contains {} servers and {} classes", servers.length, classes.length);
        String[] starters;
        try {
            starters = controlSystemHelper.getExportedDevicesForClass("Starter");
        } catch (final DevFailed e) {
            logDevFailed("getExportedDevicesForClass(Starter)", e);
            // there is no starters on this CS
            starters = new String[]{};
        }
        starterGroup = controlSystemHelper.createGroup(starters, DEFAULT_TIMEOUT);
        logger.debug("starter group created with size of {} ", starters.length);
        status = "Control system contains " + servers.length + " servers and " + classes.length + " classes\n";
        status = status + "controling " + starters.length + " starters";
    }

    @Delete
    public void delete() throws DevFailed {
        dynamicManager.clearAll();
    }

    /**
     * Execute Init command on a group of devices
     *
     * @param in The device class pattern (wildcard char is *) ,
     *           timeout(optional)
     * @throws DevFailed
     */
    @Command(inTypeDesc = "The device class pattern (wildcard char is *) on which Init command will be executed, timeout(optional)", displayLevel = DispLevel._EXPERT)
    @StateMachine(deniedStates = DeviceState.INIT)
    public void initClassForget(final String[] in) throws DevFailed {
        try {
            logs("Execute command initClassForget " + Arrays.toString(in));
            controlSystemHelper.initClass(true, in);
        } catch (DevFailed e) {
            throwDevFailed("initClassForget(" + Arrays.toString(in) + ")", e);
        }
    }

    /**
     * Execute Init command on a group of devices
     *
     * @param className The device class pattern (wildcard char is *)
     */
    @Command(inTypeDesc = "The device class pattern (wildcard char is *) on which Init command will be executed")
    @StateMachine(deniedStates = {DeviceState.INIT, DeviceState.RUNNING})
    public void initClass(final String className) {
        logs("Execute initClass (" + className + ")");
        // detach execution and log result in an attribute
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    state = DeviceState.RUNNING;
                    status = "Sending Init command to " + className + " in progress";
                    lastResult = ArrayUtils.add(controlSystemHelper.initClass(false, className), 0, getExecutionDate());
                } catch (final DevFailed e) {
                    logDevFailed("initClass (" + className + ")", e);
                } finally {
                    state = DeviceState.ON;
                    status = WAITING_FOR_REQUEST;
                }
            }
        }).start();
    }

    /**
     * Execute Init command on a group of devices
     *
     * @param className The device class pattern (wildcard char is *)
     */
    @Command(inTypeDesc = "The device class pattern (wildcard char is *) on which Init command will be executed")
    @StateMachine(deniedStates = {DeviceState.INIT, DeviceState.RUNNING})
    public void initFaultyClass(final String className) {
        logs("Execute initFaultyClass (" + className + ")");
        // detach execution and log result in an attribute
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    state = DeviceState.RUNNING;
                    status = "Sending Init command to " + className + " in progress";
                    lastResult = ArrayUtils.add(controlSystemHelper.initFaultyClass(false, className), 0,
                            getExecutionDate());
                } catch (final DevFailed e) {
                    logDevFailed("initFaultyClass (" + className + ")", e);
                } finally {
                    state = DeviceState.ON;
                    status = WAITING_FOR_REQUEST;
                }
            }
        }).start();
    }

    @Command(inTypeDesc = DESC_2)
    @StateMachine(deniedStates = {DeviceState.INIT, DeviceState.RUNNING})
    public void getDeviceDependencyTree(String devicePattern) {
        logs("Execute getDeviceDependencyTree (" + devicePattern + ")");
        // detach execution and log result in an attribute
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    state = DeviceState.RUNNING;
                    status = "getDeviceDependencyTree " + devicePattern + " in progress";
                    lastResult = buildDependencyTree(devicePattern).split("\n");
                } catch (final DevFailed e) {
                    logDevFailed("getDevicedDependencyTree (" + devicePattern + ")", e);
                } finally {
                    state = DeviceState.ON;
                    status = WAITING_FOR_REQUEST;
                }
            }
        }).start();
    }

    @Command
    @StateMachine(deniedStates = {DeviceState.INIT, DeviceState.RUNNING})
    public String getDeviceDependencyTreeSync(String devicePattern)throws DevFailed  {
        return buildDependencyTree(devicePattern);
    }

    private String buildDependencyTree(String devicePattern) throws DevFailed {
        DeviceTreeBuilder builder = new DeviceTreeBuilder();
        builder.setControlSystemHelper(controlSystemHelper);
        List<DeviceTree> trees = builder.getDeviceTrees(devicePattern);
        StringBuilder stringBuilder = new StringBuilder();
        for (DeviceTree tree : trees) {
            stringBuilder.append(tree.toStringForTango()).append("\n");
        }
        return stringBuilder.toString();
    }

    /**
     * Execute a command on a group of devices
     *
     * @param in The device class pattern (wildcard char is *) ,command name,
     *           timeout(optional)
     * @throws DevFailed
     */
    @Command(inTypeDesc = "The device class pattern (wildcard char is *) on which command will be executed,command name, timeout(optional)", displayLevel = DispLevel._EXPERT)
    @StateMachine(deniedStates = DeviceState.INIT)
    public void commandClass(final String[] in) throws DevFailed {
        logs("Execute commandClass (" + Arrays.toString(in) + ")");
        try {
            final String className = in[0];
            final String commandName = in[1];
            final int timeout = controlSystemHelper.getTimeout(in);

            final String[] devices = controlSystemHelper.getExportedDevicesForClass(className);
            logger.debug("command {} on devices {}", commandName, Arrays.toString(devices));
            controlSystemHelper.groupedCommandForget(timeout, commandName, devices);
        } catch (final DevFailed e) {
            throwDevFailed("commandClass (" + Arrays.toString(in) + ")", e);
        }

    }

    private String getExecutionDate() {
        final StringBuilder sb = new StringBuilder("execution date is ");
        final String now = getDate();
        sb.append(now).append("\n");
        return sb.toString();
    }

    private String getDate() {
        return DATE_FORMAT.format(new Date(System.currentTimeMillis()));
    }

    /**
     * Get an attribute value for devices of a class and report result in lastStateResult
     * attribute
     *
     * @param className     The device class pattern (wildcard char is *)
     * @param attributeName the attribute to reas
     * @return
     * @throws DevFailed
     */
    @Command(inTypeDesc = "The class name and attribute name", outTypeDesc = "The attribute value of all class devices")
    @StateMachine(deniedStates = {DeviceState.INIT, DeviceState.RUNNING})
    public void getClassAttributes(final String[] in) throws DevFailed {
        logs("Execute getClassAttributes (" + Arrays.toString(in) + ")");
        if (in != null && in.length > 1) {
            final String className = in[0];
            final String attributeName = in[1];

            // detach execution and log result in an attribute
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        state = DeviceState.RUNNING;
                        status = "Retrieving attribute " + attributeName + " of " + className;
                        lastResult = ArrayUtils.add(controlSystemHelper.getClassAttribute(attributeName, in), 0,
                                getExecutionDate());
                    } catch (final DevFailed e) {
                        logDevFailed("getClassAttributes (" + Arrays.toString(in) + ")", e);
                    } finally {
                        state = DeviceState.ON;
                        status = WAITING_FOR_REQUEST;
                    }
                }
            }).start();
        } else {
            String errorMessage = "Error argin is too short [Class name, Attribute Name]";
            logs(errorMessage);
            DevFailedUtils.throwDevFailed(errorMessage);
        }
    }

    /**
     * Get state for devices of a class and report result in lastStateResult
     * attribute
     *
     * @param in The device class pattern (wildcard char is *), timeout
     *           (optional)
     * @return
     * @throws DevFailed
     */
    @Command(inTypeDesc = "The class name", outTypeDesc = "The state of all class devices")
    @StateMachine(deniedStates = {DeviceState.INIT, DeviceState.RUNNING})
    public void getClassState(final String className) throws DevFailed {
        logs("Execute getClassState (" + className + ")");
        // detach execution and log result in an attribute
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    state = DeviceState.RUNNING;
                    status = "Retrieving state of " + className;
                    lastResult = ArrayUtils.add(controlSystemHelper.getClassState(className), 0, getExecutionDate());
                } catch (final DevFailed e) {
                    logDevFailed("getClassState (" + className + ")", e);
                } finally {
                    state = DeviceState.ON;
                    status = WAITING_FOR_REQUEST;
                }
            }
        }).start();
    }

    /**
     * Get state of devices
     *
     * @param in The device pattern (wildcard char is *), timeout (optional)
     * @return
     * @throws DevFailed
     */
    @Command(inTypeDesc = DESC_1, outTypeDesc = "The state of all class devices", displayLevel = DispLevel._EXPERT)
    @StateMachine(deniedStates = DeviceState.INIT)
    public String[] getClassStateSync(final String[] in) throws DevFailed {
        logs("Execute getClassStateSync (" + Arrays.toString(in) + ")");
        String[] result = null;
        try {
            result = controlSystemHelper.getClassState(in);
        } catch (final DevFailed e) {
            throwDevFailed("getClassState (" + Arrays.toString(in) + ")", e);
        }
        if (result == null) {
            result = new String[0];
        }
        return result;
    }

    /**
     * Get status of devices of a class
     *
     * @param in The device class pattern (wildcard char is *), timeout
     *           (optional)
     * @return
     * @throws DevFailed
     */
    @Command(inTypeDesc = DESC_2, outTypeDesc = "The status of all class devices")
    @StateMachine(deniedStates = {DeviceState.INIT, DeviceState.RUNNING})
    public void getClassStatus(final String className) throws DevFailed {
        logs("Execute getClassStatus (" + className + ")");
        // detach execution and log result in an attribute
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    state = DeviceState.RUNNING;
                    status = "Retrieving status of " + className;
                    lastResult = ArrayUtils.add(controlSystemHelper.getClassStatus(className), 0, getExecutionDate());
                } catch (final DevFailed e) {
                    logDevFailed("getClassStatus (" + className + ")", e);
                } finally {
                    state = DeviceState.ON;
                    status = WAITING_FOR_REQUEST;
                }
            }
        }).start();
    }

    /**
     * Get status of devices of a class
     *
     * @param in The device class pattern (wildcard char is *), timeout
     *           (optional)
     * @return
     * @throws DevFailed
     */
    @Command(inTypeDesc = DESC_2, outTypeDesc = "The status of all class devices", displayLevel = DispLevel._EXPERT)
    @StateMachine(deniedStates = DeviceState.INIT)
    public String[] getClassStatusSync(final String[] in) throws DevFailed {
        logs("Execute getClassStatusSync (" + Arrays.toString(in) + ")");
        String[] result = null;
        try {
            result = controlSystemHelper.getClassStatus(in);
        } catch (final DevFailed e) {
            throwDevFailed("getClassStatusSync (" + Arrays.toString(in) + ")", e);
        }
        if (result == null) {
            result = new String[0];
        }
        return result;
    }

    /**
     * Ping a group of devices
     *
     * @param in The device class pattern (wildcard char is *), timeout
     *           (optional)
     * @return
     * @throws DevFailed
     */
    @Command(inTypeDesc = DESC_2, outTypeDesc = "true if all devices are alive")
    @StateMachine(deniedStates = {DeviceState.INIT, DeviceState.RUNNING})
    public void pingClass(final String in) throws DevFailed {
        logs("Execute pingClass (" + in + ")");
        // detach execution and log result in an attribute
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    state = DeviceState.RUNNING;
                    status = "ping of " + in;
                    final String[] devices = controlSystemHelper.getExportedDevicesForClass(in);
                    logger.debug("ping {} devices {}", devices.length, Arrays.toString(devices));
                    final boolean hasPing = controlSystemHelper.createGroup(devices, 3000).ping(true);
                    lastResult = new String[]{getExecutionDate(), Boolean.toString(hasPing)};
                } catch (final DevFailed e) {
                    logDevFailed("pingClass (" + in + ")", e);
                } finally {
                    state = DeviceState.ON;
                    status = WAITING_FOR_REQUEST;
                }
            }
        }).start();
    }

    /**
     * Ping a group of devices
     *
     * @param in The device class pattern (wildcard char is *), timeout
     *           (optional)
     * @return
     * @throws DevFailed
     */
    @Command(inTypeDesc = DESC_2, outTypeDesc = "true if all devices are alive", displayLevel = DispLevel._EXPERT)
    @StateMachine(deniedStates = DeviceState.INIT)
    public boolean pingClassSynch(final String[] in) throws DevFailed {
        logs("Execute pingClassSynch (" + Arrays.toString(in) + ")");
        boolean ping = false;
        try {
            final String className = in[0];
            final int timeout = controlSystemHelper.getTimeout(in);
            final String[] devices = controlSystemHelper.getExportedDevicesForClass(className);
            logger.debug("ping {} devices {}", devices.length, Arrays.toString(devices));
            ping = controlSystemHelper.createGroup(devices, timeout).ping(true);
        } catch (final DevFailed e) {
            throwDevFailed("pingClassSynch (" + Arrays.toString(in) + ")", e);
        }

        return ping;
    }

    /**
     * Kill a group of servers
     *
     * @param in The device class pattern (wildcard char is *), timeout
     *           (optional)
     * @throws DevFailed
     */
    @Command(inTypeDesc = DESC_2, displayLevel = DispLevel._EXPERT)
    @StateMachine(deniedStates = DeviceState.INIT)
    public void killClass(final String[] in) throws DevFailed {
        logs("Execute killClass (" + Arrays.toString(in) + ")");
        try {
            final String className = in[0];
            final int timeout = controlSystemHelper.getTimeout(in);

            final String[] deviceNames = controlSystemHelper.getExportedDevicesForClass(className);
            killDeviceArray(deviceNames, timeout);
        } catch (final DevFailed e) {
            throwDevFailed("killClass (" + Arrays.toString(in) + ")", e);
        }
    }

    /**
     * Kill a group of servers
     *
     * @param in The device pattern (wildcard char is *), timeout (optional)
     * @throws DevFailed
     */
    @Command(inTypeDesc = DESC_1, displayLevel = DispLevel._EXPERT)
    @StateMachine(deniedStates = DeviceState.INIT)
    public void killDevices(final String[] in) throws DevFailed {
        logs("Execute killDevices (" + Arrays.toString(in) + ")");
        try {
            final String devicePattern = in[0];
            int timeout = DEFAULT_TIMEOUT;
            if (in.length > 1) {
                timeout = Integer.parseInt(in[1]);
            }

            final String[] deviceNames = controlSystemHelper.getExportedDevices(devicePattern);
            killDeviceArray(deviceNames, timeout);
        } catch (final DevFailed e) {
            throwDevFailed("killDevices (" + Arrays.toString(in) + ")", e);
        }
    }

    private void killDeviceArray(final String[] deviceNames, final int timeout) throws DevFailed {
        logger.debug("kill devices {}", Arrays.toString(deviceNames));
        logs("Execute killDeviceArray (" + Arrays.toString(deviceNames) + "," + timeout + ")");
        // retrieve admin devices of the devices
        final Set<String> adminDevices = new HashSet<String>();
        for (final String deviceName : deviceNames) {
            try {
                adminDevices.add(new DeviceProxy(deviceName).adm_name());
            } catch (final DevFailed e) {
                logDevFailed("Create DeviceProxy (" + deviceName + ")", e);
                logger.info("{} is not started, will not stop it", deviceName);
            }
        }
        logger.debug("will send kill to {}", adminDevices);
        try {
            if (adminDevices.size() > 0) {
                final Group group = controlSystemHelper.createGroup(
                        adminDevices.toArray(new String[adminDevices.size()]), timeout);
                group.command_inout_asynch("Kill", true, true);
                logger.debug("kill sent");
            }
        } catch (final DevFailed e) {
            throwDevFailed("killDeviceArray (" + Arrays.toString(deviceNames) + "," + timeout + ")", e);
        }
    }

    /**
     * Start a group of Server throught devices of Starter class.
     *
     * @param in The device class pattern (wildcard char is *), timeout
     *           (optional)
     * @throws DevFailed
     */
    @Command(inTypeDesc = DESC_2, displayLevel = DispLevel._EXPERT)
    @StateMachine(deniedStates = DeviceState.INIT)
    public void startClass(final String[] in) throws DevFailed {
        logs("Execute startClass (" + Arrays.toString(in) + ")");
        final String className = in[0];
        int timeout = DEFAULT_TIMEOUT;
        if (in.length > 1) {
            timeout = Integer.parseInt(in[1]);
        }
        try {
            starterGroup.set_timeout_millis(timeout, true);
        } catch (final DevFailed e) {
            logDevFailed("set_timeout_millis(" + timeout + ")", e);
        }
        // get all device of className
        try {
            final String[] deviceNames = tangoDB.get_device_name("*", className);
            startServers(deviceNames);
        } catch (final DevFailed e) {
            throwDevFailed("startClass (" + Arrays.toString(in) + ")", e);
        }
    }

    /**
     * Start a group of Server throught devices of Starter class.
     *
     * @param in The device pattern (wildcard char is *), timeout (optional)
     * @throws DevFailed
     */
    @Command(inTypeDesc = DESC_1, displayLevel = DispLevel._EXPERT)
    @StateMachine(deniedStates = DeviceState.INIT)
    public void startDevices(final String[] in) throws DevFailed {
        logs("Execute startDevices (" + Arrays.toString(in) + ")");
        final String devicePattern = in[0];
        final int timeout = controlSystemHelper.getTimeout(in);
        try {
            starterGroup.set_timeout_millis(timeout, true);
        } catch (final DevFailed e) {
            logDevFailed("set_timeout_millis(" + timeout + ")", e);
        }
        // retrieve servers of the devices
        try {
            final String[] deviceNames = controlSystemHelper.getDevices(devicePattern);
            startServers(deviceNames);
        } catch (final DevFailed e) {
            throwDevFailed("startDevices (" + Arrays.toString(in) + ")", e);
        }
    }

    @Command(name = "StartServer", inTypeDesc = "Server name", displayLevel = DispLevel._EXPERT)
    @StateMachine(deniedStates = DeviceState.INIT)
    public void startServer(final String server) throws DevFailed {
        logs("Execute startServer (" + server + ")");
        try {
            controlSystemHelper.startServer(server);
        } catch (final DevFailed e) {
            throwDevFailed("startServer (" + server + ")", e);
        }
    }

    @Command(name = "StopServer", inTypeDesc = "Server name", displayLevel = DispLevel._EXPERT)
    @StateMachine(deniedStates = DeviceState.INIT)
    public void stopServer(final String server) throws DevFailed {
        logs("Execute stopServer (" + server + ")");
        try {
            controlSystemHelper.stopServer(server);
        } catch (final DevFailed e) {
            throwDevFailed("stopServer (" + server + ")", e);
        }
    }

    @Command(name = "StartServerWithStarter", inTypeDesc = "Server name, Starter deviceName", displayLevel = DispLevel._EXPERT)
    @StateMachine(deniedStates = DeviceState.INIT)
    public void startServer(final String[] in) throws DevFailed {
        logs("Execute startServer (" + Arrays.toString(in) + ")");
        try {
            if (in != null && in.length > 1) {
                controlSystemHelper.startServer(in[0], in[1]);
            } else {
                DevFailedUtils.throwDevFailed("Argin is too short [Server name, Starter deviceName]");
            }
        } catch (final DevFailed e) {
            throwDevFailed("startServer (" + Arrays.toString(in) + ")", e);
        }
    }

    private synchronized void startServers(final String[] deviceNames) throws DevFailed {
        // retrieve servers of the devices
        final Set<String> serverToStart = new HashSet<String>();
        for (final String deviceName : deviceNames) {
            final String server = tangoDB.import_device(deviceName).server;
            serverToStart.add(server);
        }
        logger.debug("start servers {}", serverToStart);
        // find the starters that control this class
        final GroupAttrReplyList serverResults = starterGroup.read_attribute("Servers", true);
        for (final Object object : serverResults) {
            final GroupAttrReply reply = (GroupAttrReply) object;
            try {
                final String[] managedServers = reply.get_data().extractStringArray();
                for (final String server : serverToStart) {
                    if (Arrays.toString(managedServers).contains(server)) {
                        // starts the server on starter if it manages it
                        final DeviceData data = new DeviceData();
                        data.insert(server);
                        logger.debug("starting server {} on {}", server, reply.dev_name());
                        try {
                            starterGroup.get_device(reply.dev_name()).command_inout_asynch("DevStart", data);
                        } catch (final DevFailed e) {
                            logger.error("failed DevStart on starter {}: {}", reply.dev_name(),
                                    DevFailedUtils.toString(e));
                        }
                    }
                }
            } catch (final DevFailed e) {
                logger.error("failed reading attribute Servers on starter {}: {}", reply.dev_name(),
                        DevFailedUtils.toString(e));
            }
        }
    }

    /**
     * Execute Init command on a group of devices
     *
     * @param in The device pattern (wildcard char is *), timeout (optional)
     * @throws DevFailed
     */
    @Command(inTypeDesc = "The device pattern (wildcard char is *) on which command will be executed, timeout(optional)", displayLevel = DispLevel._EXPERT)
    @StateMachine(deniedStates = DeviceState.INIT)
    public void initDevicesForget(final String[] in) throws DevFailed {
        logs("Execute initDevicesForget (" + Arrays.toString(in) + ")");
        try {
            controlSystemHelper.initDevices(true, in);
        } catch (final DevFailed e) {
            throwDevFailed("initDevicesForget (" + Arrays.toString(in) + ")", e);
        }
    }

    /**
     * Execute Init command on a group of devices
     *
     * @param in The device pattern (wildcard char is *)
     */
    @Command(inTypeDesc = "The device pattern (wildcard char is *) on which command will be executed")
    @StateMachine(deniedStates = DeviceState.INIT)
    public void initDevices(final String in) {
        logs("Execute initDevices (" + in + ")");
        // detach execution and log result in an attribute
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    state = DeviceState.RUNNING;
                    status = "Sending Init command to " + in + " in progress";
                    lastResult = ArrayUtils.add(controlSystemHelper.initDevices(false, in), 0, getExecutionDate());
                } catch (final DevFailed e) {
                    logDevFailed("initDevices (" + in + ")", e);
                } finally {
                    state = DeviceState.ON;
                    status = WAITING_FOR_REQUEST;
                }

            }
        }).start();
    }

    /**
     * Execute a command on a group of devices
     *
     * @param in The device pattern (wildcard char is *),command name, timeout
     *           (optional)
     * @throws DevFailed
     */
    @Command(inTypeDesc = "The device pattern (wildcard char is *) on which command will be executed,command name, timeout(optional)", displayLevel = DispLevel._EXPERT)
    @StateMachine(deniedStates = DeviceState.INIT)
    public void commandDevices(final String[] in) throws DevFailed {
        logs("Execute commandDevices (" + Arrays.toString(in) + ")");
        try {
            if (in != null && in.length > 1) {
                final String devicesName = in[0];
                final String commandName = in[1];
                final int timeout = controlSystemHelper.getTimeout(in);
                final String[] devices = controlSystemHelper.getExportedDevices(devicesName);
                logger.debug("command {} on devices {}", commandName, Arrays.toString(devices));
                controlSystemHelper.groupedCommandForget(timeout, commandName, devices);
            } else {
                DevFailedUtils.throwDevFailed("Argin is too short [Class name, Command name]");
            }
        } catch (final DevFailed e) {
            throwDevFailed("commandDevices (" + Arrays.toString(in) + ")", e);
        }
    }

    /**
     * Get state of devices and report result in lastStateResult attribute
     *
     * @param deviceName The device pattern (wildcard char is *), timeout (optional)
     * @return
     * @throws DevFailed
     */
    @Command(inTypeDesc = DESC_1, outTypeDesc = "The state of all class devices")
    @StateMachine(deniedStates = DeviceState.INIT)
    public void getDevicesState(final String deviceName) throws DevFailed {
        logs("Execute getDevicesState (" + deviceName + ")");
        // detach execution and log result in an attribute
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    state = DeviceState.RUNNING;
                    status = "Retrieving state of " + deviceName;
                    lastResult = ArrayUtils.add(controlSystemHelper.getDevicesState(deviceName), 0, getExecutionDate());
                } catch (final DevFailed e) {
                    logDevFailed("getDevicesState (" + deviceName + ")", e);
                } finally {
                    state = DeviceState.ON;
                    status = WAITING_FOR_REQUEST;
                }
            }
        }).start();
    }

    /**
     * Get state of devices
     *
     * @param in The device pattern (wildcard char is *), timeout (optional)
     * @return
     * @throws DevFailed
     */
    @Command(inTypeDesc = DESC_1, outTypeDesc = "The state of all class devices", displayLevel = DispLevel._EXPERT)
    @StateMachine(deniedStates = DeviceState.INIT)
    public String[] getDevicesStateSync(final String[] in) throws DevFailed {
        logs("Execute getDevicesState (" + Arrays.toString(in) + ")");
        String[] result = null;
        try {
            result = controlSystemHelper.getDevicesState(in);
        } catch (final DevFailed e) {
            throwDevFailed("getDevicesState (" + Arrays.toString(in) + ")", e);
        }

        if (result == null) {
            result = new String[0];
        }
        return result;
    }

    /**
     * Get status of devices
     *
     * @param in The device pattern (wildcard char is *), timeout (optional)
     * @return
     * @throws DevFailed
     */
    @Command(inTypeDesc = "the device pattern", outTypeDesc = "The status of all class devices")
    @StateMachine(deniedStates = {DeviceState.INIT, DeviceState.RUNNING})
    public void getDevicesStatus(final String devicePattern) throws DevFailed {
        logs("Execute getDevicesStatus (" + devicePattern + ")");
        // detach execution and log result in an attribute
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    state = DeviceState.RUNNING;
                    status = "Retrieving status of " + devicePattern;

                    lastResult = ArrayUtils.add(controlSystemHelper.getDevicesStatus(devicePattern), 0,
                            getExecutionDate());

                } catch (final DevFailed e) {
                    logDevFailed("getDevicesStatus (" + devicePattern + ")", e);
                } finally {
                    state = DeviceState.ON;
                    status = WAITING_FOR_REQUEST;
                }
            }
        }).start();
    }

    /**
     * Get status of devices
     *
     * @param in The device pattern (wildcard char is *), timeout (optional)
     * @return
     * @throws DevFailed
     */
    @Command(inTypeDesc = DESC_1, outTypeDesc = "The status of all class devices", displayLevel = DispLevel._EXPERT)
    @StateMachine(deniedStates = DeviceState.INIT)
    public String[] getDevicesStatusSync(final String[] in) throws DevFailed {
        logs("Execute getDevicesStatusSync (" + Arrays.toString(in) + ")");
        String[] result = null;
        try {
            result = controlSystemHelper.getDevicesStatus(in);
        } catch (final DevFailed e) {
            throwDevFailed("getDevicesStatusSync (" + Arrays.toString(in) + ")", e);
        }

        if (result == null) {
            result = new String[0];
        }
        return result;
    }

    /**
     * Ping a group of devices
     *
     * @param in The device pattern (wildcard char is *), timeout (optional)
     * @return
     * @throws DevFailed
     */
    @Command(inTypeDesc = DESC_1, outTypeDesc = "true if all devices are alive", displayLevel = DispLevel._EXPERT)
    @StateMachine(deniedStates = DeviceState.INIT)
    public boolean pingDevicesSync(final String[] in) throws DevFailed {
        logs("Execute pingDevicesSync (" + Arrays.toString(in) + ")");
        boolean ping = false;
        try {
            final String devicePattern = in[0];
            int timeout = DEFAULT_TIMEOUT;
            if (in.length > 1) {
                timeout = Integer.parseInt(in[1]);
            }
            final String[] devices = controlSystemHelper.getDevices(devicePattern);
            logger.debug("ping devices {}", Arrays.toString(devices));
            ping = controlSystemHelper.createGroup(devices, timeout).ping(true);
        } catch (final DevFailed e) {
            throwDevFailed("pingDevicesSync (" + Arrays.toString(in) + ")", e);
        }

        return ping;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public DeviceState getState() {
        return state;
    }

    public void setState(final DeviceState state) {
        this.state = state;
    }

    public void setDynamicManager(final DynamicManager dynamicManager) {
        this.dynamicManager = dynamicManager;
    }

    public String[] getLastResult() {
        return lastResult;
    }

    public String[] getHistoric() {
        historic = historicArrays.toArray(new String[historicArrays.size()]);
        return historic;
    }

    private void logs(final String message) {
        String messageType = "INFO\t- ";
        if (message.toLowerCase().contains("error")) {
            messageType = "ERROR\t- ";
        }
        if (message.toLowerCase().contains("warning")) {
            messageType = "WARNING\t- ";
        }
        historicArrays.add(messageType + getDate() + "-" + message);
    }

    private void logDevFailed(String commandName, DevFailed e) {
        String errorMessage = DevFailedUtils.toString(e);
        lastResult = new String[]{getExecutionDate(), errorMessage};
        logger.error("Failed on {} {}", commandName, errorMessage);
        logs("Error " + commandName + " : " + errorMessage);
    }

    private void throwDevFailed(String commandName, DevFailed e) throws DevFailed {
        logDevFailed(commandName, e);
        throw e;
    }

}
