package fr.soleil.tango.server.cs.monitor;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceInfo;
import fr.esrf.TangoApi.DeviceProxy;
import org.tango.utils.TangoUtil;

import java.util.ArrayList;
import java.util.List;

public class DeviceTreeBuilder {
    private ControlSystemHelper controlSystemHelper;

    public void setControlSystemHelper(ControlSystemHelper controlSystemHelper) {
        this.controlSystemHelper = controlSystemHelper;
    }

    /**
     * @param parent device name.
     * @return
     */
    public DeviceTree getDeviceTree(DeviceTree.Device parent, DeviceTree parentTree) {
        // get device properties
        // System.out.println("==================");
        if (parentTree == null) {
            try {
                DeviceInfo info = controlSystemHelper.getDeviceInfo(parent.getDeviceName());
                parent.setFromDeviceInfo(info);
                parent.setState(new DeviceProxy(parent.getDeviceName()).state().toString());
            } catch (DevFailed e) {//ignore
            }
            parentTree = new DeviceTree(parent);
        }
        String[] props = new String[0];
        try {
            props = controlSystemHelper.getDevicesPropertiesValues(parent.getDeviceName());
        } catch (DevFailed e) {
        }
        //  System.out.println(parent + " props = " + Arrays.toString(props));
        for (int i = 0; i < props.length; i++) {
            String child = props[i].trim().toLowerCase();
            //System.out.println("calc child " + child);
            // TODO: refactor to use regex
            if (child.startsWith("//")) {
                // the device is on another tangodb (e.g. //172.17.22.6:20000/ANS/CA/MACHINESTATUS-DMZ)
                parentTree.addLeaf(new DeviceTree.Device(child, "another tango db"));
            } else {
                // the device or attribute name is often mixes with another string
                String[] split = child.split(",|:|;|\\s|=");
                for (int j = 0; j < split.length; j++) {
                    buildTree(parentTree, split[j]);
                }
            }

        }
        //System.out.println("###tree " + parentTree);
        return parentTree;
    }

    private void buildTree(DeviceTree parentTree, String child) {
        long count = child.chars().filter(ch -> ch == '/').count();
        // add devices to tree
        boolean isADevice = false;
        String deviceName = child;
        if (count == 3) {
            // is an attribute, get is device name
            // TODO is a command name
            try {
                deviceName = TangoUtil.getfullDeviceNameForAttribute(child);
                isADevice = true;
            } catch (DevFailed e) {
                try {
                    deviceName = TangoUtil.getFullDeviceNameForCommand(child);
                    isADevice = true;
                } catch (DevFailed e2) {
                }
            }
        }
        if (count == 2) {
            // is a device
            isADevice = true;
        }

        if (isADevice) {
            boolean containsLeaf = false;
            if (parentTree.getHead().getDeviceName().equalsIgnoreCase(deviceName)) {
                containsLeaf = true;
            } else {
                for (DeviceTree tree : parentTree.getSubTrees()) {
                    if (tree.containsLeaf(deviceName)) {
                        containsLeaf = true;
                        break;
                    }
                }
            }
            if (!containsLeaf) {
                DeviceTree.Device device = new DeviceTree.Device(deviceName);
                try {
                    DeviceInfo info = controlSystemHelper.getDeviceInfo(deviceName);
                    device.setFromDeviceInfo(info);
                    device.setState(new DeviceProxy(deviceName).state().toString());
                } catch (DevFailed e) {//ignore
                }
                DeviceTree childTree = parentTree.addLeaf(device);
                getDeviceTree(device, childTree);
            }
        }
    }

    /**
     * @param parents
     * @return
     */
    public List<DeviceTree> getDeviceTrees(String parents) throws DevFailed {
        List<DeviceTree> result = new ArrayList<>();
        String[] parentList = controlSystemHelper.getDevices(parents);
        for (String parent :
                parentList) {
            result.add(getDeviceTree(new DeviceTree.Device(parent.toLowerCase()), null));
        }
        return result;
    }
}
