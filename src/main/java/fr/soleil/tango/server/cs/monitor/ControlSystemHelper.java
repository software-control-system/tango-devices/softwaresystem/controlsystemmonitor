package fr.soleil.tango.server.cs.monitor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.esrf.TangoApi.*;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.DeviceState;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.Group.Group;
import fr.esrf.TangoApi.Group.GroupAttrReply;
import fr.esrf.TangoApi.Group.GroupAttrReplyList;
import fr.esrf.TangoApi.Group.GroupCmdReply;
import fr.esrf.TangoApi.Group.GroupCmdReplyList;
import fr.soleil.tango.clientapi.InsertExtractUtils;
import org.tango.utils.TangoUtil;

public class ControlSystemHelper {
    private static final String LOG_SEPARATOR = "####################";
    private static final int DEFAULT_TIMEOUT = 3000;
    private final Logger logger = LoggerFactory.getLogger(ControlSystemMonitor.class);
    // Starter Map <Host, StarterDeviceName>
    private static final Map<String, String> starterMap = new HashMap<String, String>();
    // Existing Host List
    private static final List<String> hostList = new ArrayList<String>();
    // Starter Map <HostName, ShortHostName>
    private static final Map<String, String> hostNameMap = new HashMap<String, String>();

    private final Database tangoDB;

    ControlSystemHelper(final Database tangoDB) {
        this.tangoDB = tangoDB;
    }

    String[] getExportedDevices(final String devicePattern) throws DevFailed {
        final String[] deviceNames = tangoDB.get_device_exported(devicePattern);
        if (deviceNames.length == 0) {
            DevFailedUtils.throwDevFailed("No device exported for pattern " + devicePattern);
        }
        return deviceNames;
    }

    String[] getDevices(final String devicePattern) throws DevFailed {
        final String[] deviceNames = tangoDB.get_device_list(devicePattern);
        if (deviceNames.length == 0) {
            DevFailedUtils.throwDevFailed("No device found for pattern " + devicePattern);
        }
        return deviceNames;
    }

    public String[] getDevicesPropertiesValues(final String deviceName) throws DevFailed {
        final String[] propsNames = tangoDB.get_device_property_list(deviceName, "*");
        List<String> propsValues = new ArrayList<>();
        //  final String[] propsValues = new String[propsNames.length];
        if (propsNames.length > 0) {
            DbDatum[] props = tangoDB.get_device_property(deviceName, propsNames);
            for (int i = 0; i < props.length; i++) {
                String[] temp = props[i].extractStringArray();
                propsValues.addAll(Arrays.asList(temp));
            }
        }
        return propsValues.toArray(new String[0]);
    }

    public DeviceInfo getDeviceInfo(String deviceName) throws DevFailed {
        return tangoDB.get_device_info(deviceName);
    }

    String[] getExportedDevicesForClass(final String className) throws DevFailed {
        final String[] deviceNames = tangoDB.get_device_exported_for_class(className);
        if (deviceNames.length == 0) {
            DevFailedUtils.throwDevFailed("No device exported for class " + className);
        }
        return deviceNames;
    }

    String[] getDevicesForClass(final String className) throws DevFailed {
        final String[] deviceNames = tangoDB.get_device_name("*", className);
        if (deviceNames.length == 0) {
            DevFailedUtils.throwDevFailed("No device found for class " + className);
        }
        return deviceNames;
    }

    Group createGroup(final String[] deviceNames, final int timeout) throws DevFailed {
        final Group group = new Group("");
        group.add(deviceNames);
        try {
            group.set_timeout_millis(timeout, true);
        } catch (final DevFailed e) {
            // ignore
        }
        return group;

    }

    String[] initDevices(final boolean forget, final String... in) throws DevFailed {
        final String devicePattern = in[0];
        final int timeout = getTimeout(in);
        final String[] devices = getDevices(devicePattern);
        return init(forget, devices, timeout);
    }

    String[] initClass(final boolean forget, final String... in) throws DevFailed {
        final String className = in[0];
        final int timeout = getTimeout(in);
        final String[] devices = getExportedDevicesForClass(className);
        return init(forget, devices, timeout);
    }

    String[] initFaultyClass(final boolean forget, final String... in) throws DevFailed {
        final String className = in[0];
        final int timeout = getTimeout(in);
        final String[] devices = getExportedDevicesForClass(className);
        return init(forget, getFaultyDevices(timeout, devices), timeout);
    }

    private String[] init(final boolean forget, final String[] devices, final int timeout) throws DevFailed {
        logger.info(LOG_SEPARATOR);
        logger.info("Init on {} devices {}", devices.length, Arrays.toString(devices));
        final String[] result = new String[devices.length];
        final GroupCmdReplyList replies = createGroup(devices, timeout).command_inout("Init", forget);
        if (!forget) {
            // get data to throw e
            int i = 0;
            for (final Object obj : replies) {
                try {
                    ((GroupCmdReply) obj).get_data();
                    result[i] = devices[i] + ": OK";
                } catch (final DevFailed e) {
                    result[i] = devices[i] + ": " + DevFailedUtils.toString(e);
                    logger.error("Failed to init {}: ", devices[i]);
                    DevFailedUtils.logDevFailed(e, logger);
                }
                i++;
            }
        }
        logger.info("Init done");
        return result;
    }

    int getTimeout(final String[] in) {
        int timeout = DEFAULT_TIMEOUT;
        if (in.length > 1) {
            try {
                timeout = Integer.parseInt(in[in.length - 1]);
            } catch (final NumberFormatException e) {
                // ignore
            }
        }
        return timeout;
    }

    String[] getDevicesState(final String... in) throws DevFailed {

        final int timeout = getTimeout(in);

        final String devicePattern = in[0];
        final String[] devices = getDevices(devicePattern);
        final String[] result = getState(timeout, devices);
        return result;
    }

    String[] getClassState(final String... in) throws DevFailed {
        final int timeout = getTimeout(in);
        final String devicePattern = in[0];
        final String[] devices = getDevicesForClass(devicePattern);
        final String[] result = getState(timeout, devices);
        return result;
    }

    String[] getClassAttribute(final String attributeName, final String... in) throws DevFailed {
        final int timeout = getTimeout(in);
        final String devicePattern = in[0];
        final String[] devices = getDevicesForClass(devicePattern);
        final String[] result = getAttribute(timeout, attributeName, devices);
        return result;
    }

    private String[] getAttribute(final int timeout, final String attributeName, final String[] devices)
            throws DevFailed {
        logger.info(LOG_SEPARATOR);
        logger.info("Attributes {} of devices {}", attributeName, Arrays.toString(devices));
        final String[] result = groupedAttribute(timeout, attributeName, devices);
        logger.info("states are: ");
        for (final String string : result) {
            logger.info("{} ", string);
        }

        return result;
    }

    private String[] getState(final int timeout, final String[] devices) throws DevFailed {
        logger.info(LOG_SEPARATOR);
        logger.info("State on {} devices {}", devices.length, Arrays.toString(devices));
        final String[] result = groupedState(timeout, devices);
        logger.info("states are: ");
        for (final String string : result) {
            logger.info("{} ", string);
        }

        return result;
    }

    public String[] getDevicesStatus(final String... in) throws DevFailed {
        final String devicePattern = in[0];
        final int timeout = getTimeout(in);
        final String[] devices = getDevices(devicePattern);
        logger.info(LOG_SEPARATOR);
        logger.info("Status on {} devices {}", devices.length, Arrays.toString(devices));

        final String[] result = groupedStatus(timeout, devices);
        logger.info("status are: ");
        for (final String string : result) {
            logger.info("{} ", string);
        }
        return result;
    }

    public String[] getClassStatus(final String... in) throws DevFailed {
        final String className = in[0];
        final int timeout = getTimeout(in);
        final String[] devices = getExportedDevicesForClass(className);
        logger.info(LOG_SEPARATOR);
        logger.info("Status on {} devices {}", devices.length, Arrays.toString(devices));

        final String[] result = groupedStatus(timeout, devices);
        logger.info("status are: ");
        for (final String string : result) {
            logger.info("{} ", string);
        }
        return result;
    }

    public void startServer(final String server) throws DevFailed {
        if (tangoDB != null && server != null) {
            DbServInfo get_server_info = tangoDB.get_server_info(server);
            DeviceProxy deviceProxy = getStarter(get_server_info);
            if (deviceProxy != null) {
                DeviceData devData = new DeviceData();
                devData.insert(server);
                deviceProxy.command_inout("DevStart", devData);
            }
        } else {
            DevFailedUtils.throwDevFailed("Server is not defined");
        }
    }

    private String getShortHostName(String hostName) throws DevFailed {
        String shortHost = hostNameMap.get(hostName.toLowerCase());
        if (shortHost == null) {
            if (hostList.isEmpty()) {
                if (tangoDB != null) {
                    String[] host_name_list = tangoDB.get_host_list();
                    if (host_name_list != null && host_name_list.length > 0) {
                        for (String host : host_name_list) {
                            hostList.add(host.toLowerCase());
                        }
                    }
                }
            }

            for (String host : hostList) {
                // System.out.println("*****************host " + host);
                // System.out.println("*****************hostName " + hostName);
                if (host != null && !host.isEmpty() && hostName.startsWith(host) && host.length() < hostName.length()) {
                    shortHost = host;
                    break;
                }
            }

            if (shortHost == null || shortHost.isEmpty()) {
                shortHost = hostName.toLowerCase();
            }

            hostNameMap.put(hostName.toLowerCase(), shortHost);
        }

        //System.out.println("shortHost=" + shortHost);
        return shortHost;
    }

    public void startServer(final String server, final String starterDeviceName) throws DevFailed {
        if (tangoDB != null && server != null) {
            DeviceProxy deviceProxy = DeviceProxyFactory.get(starterDeviceName);
            if (deviceProxy != null) {
                DeviceData devData = new DeviceData();
                devData.insert(server);
                deviceProxy.command_inout("DevStart", devData);
            } else {
                DevFailedUtils.throwDevFailed("Starter is not found " + starterDeviceName);
            }
        } else {
            DevFailedUtils.throwDevFailed("Server is not defined");
        }
    }

    public void stopServer(final String server) throws DevFailed {
        if (tangoDB != null && server != null) {
            DbServInfo get_server_info = tangoDB.get_server_info(server);
            DeviceProxy deviceProxy = getStarter(get_server_info);
            if (deviceProxy != null) {
                DeviceData devData = new DeviceData();
                devData.insert(server);
                deviceProxy.command_inout("DevStop", devData);
            }
        } else {
            DevFailedUtils.throwDevFailed("Server is not defined");
        }
    }

    private String getStarterDeviceName(final String hostName) throws DevFailed {
        String starterDeviceName = starterMap.get(hostName.toLowerCase());
        if (starterDeviceName == null) {
            // System.out.println("hostName=" + hostName);
            if (tangoDB != null) {
                String shortHost = getShortHostName(hostName);
                String starterServerName = "starter/" + shortHost + "*";
                String[] device_name_list = tangoDB.get_device_name(starterServerName, "starter");
                if (device_name_list != null && device_name_list.length > 0 && !device_name_list[0].isEmpty()) {
                    starterDeviceName = device_name_list[0];
                    starterMap.put(hostName.toLowerCase(), starterDeviceName);
                }
            }
            if (starterDeviceName == null) {
                starterMap.put(hostName.toLowerCase(), "");
            }
            if (starterDeviceName == null) {
                starterDeviceName = "";
            }
        }

        return starterDeviceName;
    }

    public DeviceProxy getStarter(final DbServInfo server) throws DevFailed {
        DeviceProxy deviceProxy = null;
        if (tangoDB != null && server != null) {
            String host = server.host;
            String starterDeviceName = getStarterDeviceName(host);
            if (starterDeviceName == null || starterDeviceName.isEmpty()) {
                DevFailedUtils.throwDevFailed("Starter device is not found on host " + host);
            } else {
                deviceProxy = DeviceProxyFactory.get(starterDeviceName);
            }
        } else {
            DevFailedUtils.throwDevFailed("Server is not defined");
        }

        return deviceProxy;
    }

    void groupedCommandForget(final int timeout, final String commandName, final String[] devices) throws DevFailed {
        createGroup(devices, timeout).command_inout_asynch(commandName, true, true);
    }

    String[] getFaultyDevices(final int timeout, final String[] devices) throws DevFailed {
        int i = 0;
        final Set<String> faultyDevices = new HashSet<String>();
        final GroupCmdReplyList cmdResults = createGroup(devices, timeout).command_inout("State", true);
        for (final Object object : cmdResults) {
            try {
                final GroupCmdReply cmdResult = (GroupCmdReply) object;
                final DevState state = cmdResult.get_data().extractDevState();
                System.out.println(devices[i] + ":" + state);
                if (state.value() == DevState._FAULT) {
                    faultyDevices.add(devices[i]);
                }
            } catch (final DevFailed e) {
                faultyDevices.add(devices[i]);
            }
            i++;
        }
        System.out.println("fault " + faultyDevices.size() + " out of " + i);
        return faultyDevices.toArray(new String[faultyDevices.size()]);
    }

    String[] groupedAttribute(final int timeout, final String attributeName, final String[] devices) throws DevFailed {
        int i = 0;
        final GroupAttrReplyList cmdResults = createGroup(devices, timeout).read_attribute(attributeName, true);
        final String[] result = new String[cmdResults.size()];
        for (final Object object : cmdResults) {
            try {
                final GroupAttrReply cmdResult = (GroupAttrReply) object;
                final DeviceAttribute data = cmdResult.get_data();
                final AttrDataFormat format = data.getDataFormat();
                final Object value = InsertExtractUtils.extractRead(data, format);
                // if (format.equals(AttrDataFormat.SCALAR)) {
                // result[i] = devices[i] + ": " + value.toString();
                // } else {
                result[i] = devices[i] + "/" + attributeName + ": " + ArrayUtils.toString(value);
                // }

            } catch (final DevFailed e) {
                result[i] = devices[i] + ": " + DevFailedUtils.toString(e);
            }
            i++;
        }
        return result;
    }

    String[] groupedState(final int timeout, final String[] devices) throws DevFailed {
        int i = 0;
        final GroupCmdReplyList cmdResults = createGroup(devices, timeout).command_inout("State", true);
        final String[] result = new String[cmdResults.size()];
        for (final Object object : cmdResults) {
            try {
                final GroupCmdReply cmdResult = (GroupCmdReply) object;
                final DevState state = cmdResult.get_data().extractDevState();
                result[i] = devices[i] + ": " + DeviceState.toString(state);
            } catch (final DevFailed e) {
                result[i] = devices[i] + ": " + DevFailedUtils.toString(e);
            }
            i++;
        }
        return result;
    }

    String[] groupedStatus(final int timeout, final String[] devices) throws DevFailed {
        int i = 0;
        final GroupCmdReplyList cmdResults = createGroup(devices, timeout).command_inout("Status", true);
        final String[] result = new String[cmdResults.size()];
        for (final Object object : cmdResults) {
            try {
                final GroupCmdReply cmdResult = (GroupCmdReply) object;
                result[i] = devices[i] + ": " + cmdResult.get_data().extractString();
            } catch (final DevFailed e) {
                result[i] = devices[i] + ": " + DevFailedUtils.toString(e);
            }
            i++;
        }
        return result;
    }

}
