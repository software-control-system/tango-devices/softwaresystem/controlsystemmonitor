package fr.soleil.tango.server.cs.monitor;


import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.DeviceInfo;

import java.util.*;

public class DeviceTree {

    public static class Device {

        private String deviceName;
        private String serverName = "N/A";
        private int pid;
        private String hostName;
        private boolean isExported;
        private String state = "N/A";

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getLastExported() {
            return lastExported;
        }

        public void setLastExported(String lastExported) {
            this.lastExported = lastExported;
        }

        private String lastExported;
        private String lastUnexported;

        public Device(String deviceName) {
            this.deviceName = deviceName;
        }

        public Device(String deviceName, String serverName) {
            this.deviceName = deviceName;
            this.serverName = serverName;
        }


        public String getDeviceName() {
            return deviceName;
        }

        public void setDeviceName(String deviceName) {
            this.deviceName = deviceName;
        }

        public String getServerName() {
            return serverName;
        }

        public void setServerName(String serverName) {
            this.serverName = serverName;
        }

        public void setFromDeviceInfo(DeviceInfo info) {
            serverName = info.server;
            pid = info.pid;
            hostName = info.hostname;
            isExported = info.exported;
            lastExported = info.last_exported;
            lastUnexported = info.last_unexported;
        }


        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(deviceName).append(" is ").append(state).append(" (").append(serverName);
            if (isExported) {
                sb.append(" is exported on ").append(hostName).append(", pid ").append(pid);
            } else {
                sb.append(" - not exported");
            }
            sb.append(")");

            return sb.toString();
        }
    }

    private Device head;

    private List<DeviceTree> leafs = new ArrayList<>();

    private DeviceTree parent = null;

    private Map<String, DeviceTree> locate = new HashMap<>();

    public DeviceTree(Device head) {
        this.head = head;
        locate.put(head.getDeviceName(), this);
    }

    public void addLeaf(Device root, Device leaf) {
        if (locate.containsKey(root.getDeviceName())) {
            locate.get(root.getDeviceName()).addLeaf(leaf);
        } else {
            addLeaf(root).addLeaf(leaf);
        }
    }

    public boolean containsLeaf(Device leaf) {
        return locate.containsKey(leaf.getDeviceName());
    }

    public boolean containsLeaf(String deviceName) {
        return locate.containsKey(deviceName);
    }

    public DeviceTree addLeaf(Device leaf) {
        DeviceTree t = new DeviceTree(leaf);
        leafs.add(t);
        t.parent = this;
        t.locate = this.locate;
        locate.put(leaf.getDeviceName(), t);
        return t;
    }

    public DeviceTree setAsParent(Device parentRoot) {
        DeviceTree t = new DeviceTree(parentRoot);
        t.leafs.add(this);
        this.parent = t;
        t.locate = this.locate;
        t.locate.put(head.getDeviceName(), this);
        t.locate.put(parentRoot.getDeviceName(), t);
        return t;
    }

    public Device getHead() {
        return head;
    }

    public DeviceTree getTree(String element) {
        return locate.get(element);
    }

    public DeviceTree getParent() {
        return parent;
    }

    public Collection<Device> getSuccessors(String root) {
        Collection<Device> successors = new ArrayList<>();
        DeviceTree tree = getTree(root);
        if (null != tree) {
            for (DeviceTree leaf : tree.leafs) {
                successors.add(leaf.head);
            }
        }
        return successors;
    }

    public Collection<DeviceTree> getSubTrees() {
        return leafs;
    }

    public static Collection<Device> getSuccessors(String of, Collection<DeviceTree> in) {
        for (DeviceTree tree : in) {
            if (tree.locate.containsKey(of)) {
                return tree.getSuccessors(of);
            }
        }
        return new ArrayList<Device>();
    }

    @Override
    public String toString() {
        return printTree(0);
    }

    public String toStringForTango() {
        return printTreeForTango(0) + "\n";
    }

    private String printTreeForTango(int increment) {
        String s = "";
        String inc = "";
        for (int i = 0; i < increment; ++i) {
            inc = inc + "| ";
        }
        s = inc + "|_" + head;
        for (DeviceTree child : leafs) {
            s += "\n" + child.printTreeForTango(increment + indent);
        }
        return s;
    }

    private static final int indent = 1;

    private String printTree(int increment) {
        String s = "";
        String inc = "";
        for (int i = 0; i < increment; ++i) {
            inc = inc + "│    ";
        }
        s = inc + "└──" + head;
        for (DeviceTree child : leafs) {
            s += "\n" + child.printTree(increment + indent);
        }
        return s;
    }
}