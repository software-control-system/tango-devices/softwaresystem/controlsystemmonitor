package fr.soleil.tango.server.cs.monitor;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import org.junit.Ignore;
import org.junit.Test;
import org.tango.utils.DevFailedUtils;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.equalTo;

import java.util.List;

public class DeviceTreeTest {

    @Test
    public void testTree() {
        DeviceTree tree = new DeviceTree(new DeviceTree.Device("parent"));
        tree.addLeaf(new DeviceTree.Device("child"));
        tree.addLeaf(new DeviceTree.Device("child"), new DeviceTree.Device("smallchild"));
        tree.addLeaf(new DeviceTree.Device("child2"));
        System.out.println(tree);
        assertThat(tree.toString(), is(equalTo("└──parent is N/A (N/A - not exported)\n" +
                "│    └──child is N/A (N/A - not exported)\n" +
                "│    │    └──smallchild is N/A (N/A - not exported)\n" +
                "│    └──child2 is N/A (N/A - not exported)")));
    }

    @Test
    @Ignore
    public void tangoTree() {
        DeviceTreeBuilder builder = new DeviceTreeBuilder();
        try {
            builder.setControlSystemHelper(new ControlSystemHelper(ApiUtil.get_db_obj()));
            List<DeviceTree> trees = builder.getDeviceTrees("tango/*");
            StringBuilder stringBuilder = new StringBuilder();
            for (DeviceTree tree : trees) {
                stringBuilder.append(tree.toStringForTango()).append("\n");
            }
            System.out.println(stringBuilder);
        } catch (DevFailed devFailed) {
            devFailed.printStackTrace();
            DevFailedUtils.printDevFailed(devFailed);
        }
    }
}
